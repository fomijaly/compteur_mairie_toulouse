import React from "react";
import { useState } from "react";
import "./Compteur.css"

function Compteur(event){
    const [count, setCount] = useState(0) //Appel de la fonction useState pour ajouter une variable d'état, on la définit par défaut à 0

    return(
        <>
        <div className="each_compteur">
            <img className="image_compteur" src={event.image} alt={event.title} /> {/*On appelle ici les props event.image et event.title pour afficher l'img et le titre*/}
            <section>
                <h3>{event.title}</h3> {/* On appelle ici la prop event.title pour afficher le titre */}
                <p>{count > 0 ? "Il y a " + count + " participants" : "Il n'y a pas de participant"}</p> {/* On fais une ternaire pour modifier le texte affiché, s'il y a des participants on en affiche le compte */}
                <div className="buttons_compteurs">
                    <button onClick={() => setCount(count + 1)}> Ajouter</button> {/* On rajoute 1 au compte grâce à la fonction setCount */}
                    <button onClick={() => setCount(count > 0 ? count - 1 : 0)}> Retirer</button> {/* Si le compte de participants est supérieur à 0, à chaque clic on supprime 1 sinon le compte est égal à 0 */}
                    <button onClick={() => setCount(count  === 0)}> Réinitialiser </button> {/* On réinitialise le compte de participants à 0 */}
                </div>
            </section>
        </div>
        </>
    )
}
export default Compteur
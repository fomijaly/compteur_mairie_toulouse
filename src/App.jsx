import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';
import './App.css'
import Compteur from '../components/Compteur'
import gif from './assets/modal.gif'

Modal.setAppElement('#root');
function App() {
  const [modalIsOpen, setModalIsOpen] = useState(() => {
    const modalHasBeenShown = localStorage.getItem('modalHasBeenShown');
    return !modalHasBeenShown;
  });

  const closeModal = () => {
    setModalIsOpen(false);
    localStorage.setItem('modalHasBeenShown', 'true');
  };

  return (
    <>
      <h1>Compte des participants </h1>
      <section className='toulouse_event'>
        <Compteur title={"Octobre rose"} image={"https://tinyurl.com/ytresna8"}/> {/* On donne les valeurs aux props */}
        <Compteur title={"Rock Symphony"} image={"https://tinyurl.com/34sr4s2f"}/>
        <Compteur title={"Cassoulet Party"} image={"https://tinyurl.com/4rcaa2dp"}/>
        <Compteur title={"Match TFC - Castres"} image={"https://tinyurl.com/mvm7eryd"}/>
      </section>

      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={{
          overlay: {
            backgroundColor: 'rgba(0, 0, 0, 0.75)'
          },
          content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            padding: '1em 4em'
          }
        }}
      >
        <div className='modalContent'>
          <div className='modalTexts'>
            <h2>✨ A savoir </h2>
            <p>Ce projet a été réalisé dans l'objectif de me former et de découvrir React.</p>
            <button className='btnModal' onClick={closeModal}>Ok, c'est noté ! 👌🏽</button>
          </div>
          <img className='imgPopUp' src={gif} alt="" />
        </div>
      </Modal>
    </>
  )
}

export default App
